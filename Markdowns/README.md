# AP EURO PANIC TIME

### Unit 1: Renaissance and Exploration
#### *May 10*
- [Fiveable](https://fiveable.me/ap-euro/unit-1) `25:00`
- [Marco](https://www.youtube.com/watch?v=DcCojSiNAqc) `9:20`
- [CrashCourse](https://www.youtube.com/watch?v=tecocKSclwc) `11:45`

### Unit 2: Age of Reformation
#### *May 11 | 10 hours*

### Unit 3: Absolutism and Constitutionalism
#### *May 12 | 10 hours*

### Unit 4: Scientific, Philosophical, and Political Developments
#### *May 12 | 10 hours*

### Unit 5: Conflict, Crisis, and Reaction in the Late 18th Century
#### *May 13 | 10 hours*

### Unit 6: Industrialization and Its Effects
#### *May 14 | 10 hours*

### Unit 7: 19th-Century Perspectives and Political Developments
#### *May 15 | 10 hours*

### Unit 8: 20th-Century Global Conflicts
#### *May 16 | 10 hours*

### Unit 9: Cold War and Contemporary Europe
#### *May 17 | 10 hours*


## Final Review
#### *May 17 | 10 hours*
- [Review Chart](https://drive.google.com/file/d/1EwnT1Tmu0Ad8jy7z9mhW48m4Ahxt2oLA/view)
- [Cram Chart](https://drive.google.com/file/d/1765JrQnabBRRhi6VOEM46IiBej3KT6Po/view)
