# APUSH PANIC TIME

### Unit 1: Early Contact with the New World (1491-1607)
#### *April 26 | 4%–6%*
Native American societies before European contact  
European exploration in the New World  
The Columbian Exchange  
Labor, slavery, and caste in the Spanish colonial system  
Cultural interactions between Europeans, Native Americans, and Africans  
- [Heimler Review](https://www.youtube.com/watch?v=jqf_c9Pw8gs) `8:44`
- [Fiveable](https://library.fiveable.me/apush/unit-1) `25:00`
- [Crash Course: Native Americans](https://youtu.be/6E9WU9TGrec?t=29) `7:14`
- [Heimler: Native Americans](https://youtu.be/X_3bH6FJsLA?t=34) `3:41`
- [Khan: Native Societies Quiz](https://www.khanacademy.org/humanities/ap-us-history/period-1/apush-before-contact/e/pre-contact-native-societies?modal=1) `4:00`
- [Heimler: European Exploration](https://youtu.be/TRrnYm_SwHc) `2:29`
- [Khan: Consequences of Columbus's voyage](https://www.khanacademy.org/humanities/ap-us-history/period-1/columbian-exchange-spanish-exploration-and-conquest/v/consequences-of-columbuss-voyage-on-the-tainos-and-europe) `6:52`
- [Heimler: Columbian Exchange](https://youtu.be/eLGxEcr-chw?t=16) `3:34`
- [Heimler: Labor, Slavery, Caste](https://youtu.be/f1h-9W2Snik) `4:16`
- [Crash Course: Colonizing America](https://youtu.be/o69TvQqyGdg?t=43) `7:48`
- [Khan: Environmental and health effects](https://www.khanacademy.org/humanities/ap-us-history/period-1/columbian-exchange-spanish-exploration-and-conquest/a/environmental-and-health-effects-of-contact) `3:00`
- [Lost Colony of Roanoke](https://www.youtube.com/watch?v=iTOKRWgjOlg) `27:52` *Optional*
- [Heimler: Cultural Interactions](https://youtu.be/s6hfwcgHvRY) `3:49`
- [Khan: Effects of voyages](https://www.khanacademy.org/humanities/ap-us-history/period-1/causation-in-period-1/v/effects-of-transatlantic-voyages) `4:56`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-history/period-1/test/period-1-unit-test?modal=1) `11:00`
- [Tom Richey Review](https://youtu.be/fkEh03wKKBg?t=158) `36:44`
- Primary Sources:
    - [Bartolomé de Las Casas debate](https://www.gilderlehrman.org/history-resources/spotlight-primary-source/bartolome-de-las-casas-debates-subjugation-indians-1550) `2:00`
    - [Columbus reports](https://www.gilderlehrman.org/history-resources/spotlight-primary-source/columbus-reports-his-first-voyage-1493) `3:00`

### Unit 2: Colonization of North America (1607-1754)
#### *April 27 | 6%–8%*
How different European colonies developed and expanded  
Transatlantic trade  
Interactions between American Indians and Europeans  
Slavery in the British colonies  
Colonial society and culture  
- [Heimler Review](https://youtu.be/wq2jG_Ww_xc) `12:11`
- [Fiveable](https://library.fiveable.me/apush/unit-2) `25:00`
- [Heimler: European Colonization](https://youtu.be/j5Y2SiZUtKY) `3:47`
- [Tom Richey: General Patton](https://youtu.be/EdV98rKV5gM) `3:02`
- [Heimler: British Colony Regions](https://youtu.be/F6HbzbkKaB4) `5:10`
- [Tom Richey: New Netherland](https://youtu.be/rH1uGY16WJM) `5:58`
- [Khan: British Colony Regions](https://www.khanacademy.org/humanities/ap-us-history/period-2/regions-of-british-colonies/e/regions-of-british-colonies) `4:00`
- [Tom Richey: Virginia Colony](https://youtu.be/BvokFncxkh8) `6:52`
- [Tom Richey: Virginia Colony Pt. 2](https://youtu.be/Up0epkGeyPY) `5:50`
- [Crash Course: Quakers, Dutch, Ladies](https://youtu.be/p47tZLJbdag?t=37) `7:20`
- [Heimler: Trans-Atlantic Trade](https://youtu.be/yPyzIqfzx8M) `3:17`
- [Tom Richey: 1st Great Awakening](https://youtu.be/CyiGmnZB-SQ) `13:37`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-history/period-2/test/period-2-unit-test?modal=1) `14:00`
- [Tom Richey Review](https://youtu.be/7zoUM1RkAng?t=247) `34:49`
- [AP Period 1 & 2 Review](https://youtu.be/ekWm3nMm-ns?t=245) `30:26`
- Primary Sources:
    - [Salem Witch Trials](https://www.gilderlehrman.org/history-resources/spotlight-primary-source/cotton-mathers-account-salem-witch-trials-1693) `3:00`
    - [William Penn](https://www.gilderlehrman.org/history-resources/spotlight-primary-source/william-penn-well-governing-my-family-1751) `4:00`
    - [New York Conspiracy](https://www.gilderlehrman.org/history-resources/spotlight-primary-source/new-york-conspiracy-1741) `3:00`

### Unit 3: Conflict and American Independence (1754-1800)
#### *April 28 | 10%–17%*
The Seven Years’ War  
The American Revolution  
The Articles of Confederation  
The creation and ratification of the Constitution  
Developing an American identity  
Immigration to and migration within America  
- [Heimler Review](https://www.youtube.com/watch?v=YhqXNJngvTo) `17:09`
- [Fiveable](https://library.fiveable.me/apush/unit-3) `25:00`
- [Tom Richey: French & Indian War](https://youtu.be/4yEBwbSh0lU) `6:10`
- [Crash Course: 7 Years War & Great Awakening](https://youtu.be/5vKGU3aEGss) `7:06`
- [Tom Richey: Parliament Taxes](https://youtu.be/dV0I2WvpGRw) `12:23`
- [Heimler: Philosphical Foundations](https://youtu.be/byLsBFDvsEE) `4:23`
- [Heimler: Revolution](https://youtu.be/1EMkWD0hr5E) `3:18`
- [Tom Richey: Battle of Trenton](https://youtu.be/EsHWAPKjZz0) `5:29`
- [Heimler: Revolutionary Ideals](https://youtu.be/NTBVFYI7mJ8) `3:43`
- [Heimler: Constitutional Convention](https://youtu.be/jN_scMGJ5_4) `4:48`
- [Tom Richey: Anti-Federalists](https://youtu.be/tKdCGXtCctw) `4:58`
- [Crash Course: Constitution](https://youtu.be/bO7FQsCcbD8) `8:42`
- [Tom Richey: Jefferson vs Hamilton](https://youtu.be/Zt4lmLK_OUc) `7:02`
- [Heimler: New Republic](https://youtu.be/eCoFYgZ0Pf0) `5:43`
- [Washington's Foreign Policy](https://youtu.be/7Qc0RCwr8Ig) `9:50`
- [Khan: Continuity & Change](https://www.khanacademy.org/humanities/ap-us-history/period-3/continuity-and-change-in-period-3/v/continuity-and-change-in-american-society-1754-1800) `6:07`
- [AP Review](https://youtu.be/XRVnLTu8r9I?t=235) `31:06`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-history/period-3/test/period-3-unit-test?modal=1) `10:00`
- [Tom Richey Review](https://youtu.be/W58wC8Mw8i0?t=275) `37:27`
- Primary Sources:
    - [The Whiskey Rebellion](https://www.gilderlehrman.org/history-resources/spotlight-primary-source/whiskey-rebellion-1794)
    - [The Sedition Act](https://www.gilderlehrman.org/history-resources/spotlight-primary-source/sedition-act-1798)

### Unit 4: Beginnings of Modern American Democracy (1800-1848)
#### *April 29 | 10%–17%*
The rise of political parties  
American foreign policy  
Innovations in technology, agriculture, and business  
Debates about federal power  
The Second Great Awakening  
Reform movements  
The experience of African Americans  
- [Heimler Review](https://www.youtube.com/watch?v=-wnDpr9PMnc) `20:41`
- [Fiveable](https://library.fiveable.me/apush/unit-4) `25:00`
- [Tom Richey: Election of 1800](https://youtu.be/kIgyxFZBRsI) `4:50`
- [Crash Course: Thomas Jefferson](https://youtu.be/_3Ox6vGteek) `8:52`
- [Tom Richey: Jefferson & Lousiana](https://youtu.be/7oGERUcPEo8) `3:31`
- [Heimler: America in the World](https://youtu.be/Xp5EncomH5k) `2:59`
- [Heimler: Politics and Regions](https://youtu.be/Cn4DLEeZLGQ) `3:00`
- [Heimler: Market Revolution Effect](https://youtu.be/oK-wET62n-Y) `3:05`
- [Tom Richey: Jefferson & Slavery](https://youtu.be/6-bxv9pNy0w) `7:11`
- [Tom Richey: Jacksonian Democracy](https://youtu.be/ZDXJny0bMmQ) `10:05`
- [Khan: Nullification Crisis](https://www.khanacademy.org/humanities/ap-us-history/period-4/apush-jackson-and-federal-power-lesson/a/the-nullification-crisis) `5:00`
- [Heimler: Second Great Awakening](https://youtu.be/_GvF6wUQzL4) `2:28`
- [Heimler: Age of Reform](https://youtu.be/_wf6-Ir55TI) `4:41`
- [Khan: Causation](https://www.khanacademy.org/humanities/ap-us-history/period-4/apush-causation-in-the-period-from-1800-to-1848-lesson/v/developing-an-american-identity-1800-1848) `4:54`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-history/period-4/test/period-4-unit-test?modal=1) `12:00`
- [Tom Richey Review](https://youtu.be/OGCzu7TpuRY?t=288) `36:08`

### Unit 5: Toward the Civil War and Reconstruction (1844-1877)
#### *April 30 | 10%–17%*
Manifest Destiny  
The Mexican–American War  
Attempts to resolve conflicts over the spread of slavery  
The election of 1860 and Southern secession  
The Civil War  
Reconstruction  
- [Fiveable](https://library.fiveable.me/apush/unit-5) `25:00`
- [Khan: Mexican-American War](https://www.khanacademy.org/humanities/ap-us-history/period-5/apush-mexican-american-war-lesson/v/the-mexican-american-war) `5:06`
- [Heimler: Compromise of 1850](https://youtu.be/aHIZONFF4nk) `4:46`
- [Tom Richey: Compromise of 1850 Rap](https://youtu.be/AOC9vlENzxk) `1:16`
- [Khan: Scott v. Sandford](https://www.khanacademy.org/humanities/ap-us-history/period-5/apush-failure-of-compromise-lesson/v/dred-scott-v-sandford) `12:28`
- [Khan: Uncle Tom's Cabin](https://www.khanacademy.org/humanities/ap-us-history/period-5/apush-sectional-conflict-regional-differences/v/uncle-toms-cabin-part-3) `4:46`
- [Tom Richey: Emancipation Proclamation](https://youtu.be/02jsgp6UQdY) `12:49`
- [Heimler: Failure of Reconstruction](https://youtu.be/wmmyPpWGK28) `4:43`
- [Slavery vs Sharecropping](https://www.youtube.com/watch?v=EaF8s01ikIg) `6:27`
- [Khan: Comparing Effects of Civil War](https://www.khanacademy.org/humanities/ap-us-history/period-5/apush-comparison-in-the-period-from-1844-to-1877-lesson/v/comparing-the-effects-of-the-civil-war-on-american-national-identity) `5:39`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-history/period-5/test/period-5-unit-test?modal=1) `10:00`
- [Tom Richey Prep](https://www.youtube.com/watch?v=6wJOEE82sdQ) `39:12`
- [AP Period 4 & 5 Review](https://youtu.be/sK2BescvUu8?t=176) `36:04`

### Unit 6: The Industrial Revolution (1865-1898)
#### *May 1 | 10%–17%*
The settlement of the West  
The "New South"  
The rise of industrial capitalism  
Immigration and migration  
Reform movements  
Debates about the role of government  
- [Fiveable](https://library.fiveable.me/apush/unit-6) `25:00`
- [Khan: Gold Rush](https://www.khanacademy.org/humanities/ap-us-history/period-6/apush-america-in-transition-setting-the-stage-lesson/a/the-gold-rush) `3:00`
- [Heimler: Westward Expansion](https://youtu.be/OAyajS_REpA) `4:55`
- [Heimler: New South](https://youtu.be/t9FZzMo8R-U) `4:05`
- [Crash Course: Gilded Age Politics](https://youtu.be/Spgdy3HkcSs) `9:14`
- [Heimler: Gilded Age Immigration](https://youtu.be/H3FaGRRhszg) `3:51`
- [Heimler: Gilded Age Reform](https://youtu.be/tTbUP8bc0jY) `4:10`
- [Tom Richey: American Federation of Labor](https://youtu.be/BQI_BldZeQk) `3:41`
- [Tom Richey: Vertical vs Horizontal Integration](https://youtu.be/6e-s9c35igU) `2:23`
- [Khan: Continuity & Change](https://www.khanacademy.org/humanities/ap-us-history/period-6/apush-continuity-and-change-over-time-in-the-period-from-1865-to-1898-lesson/v/continuity-and-change-in-the-gilded-age) `6:42`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-history/period-6/test/period-6-unit-test?modal=1) `10:00`
- [AP Review](https://youtu.be/nvYbgnr65Oo?t=75) `17:40`
- [Tom Richey Prep](https://youtu.be/fkEh03wKKBg?t=158) `36:44`

### Unit 7: The Early 20th Century (1890-1945)
#### *May 2 | 10%–17%*
Debates over imperialism  
The Progressive movement  
World War I  
Innovations in communications and technology in the 1920s  
The Great Depression and the New Deal  
World War II  
Postwar diplomacy  
- [Fiveable](https://library.fiveable.me/apush/unit-7) `25:00`
- [Heimler: Spanish-American War](https://youtu.be/CI5r-4QFLvI) `3:48`
- [Crash Course: WWI](https://youtu.be/y59wErqg4Xg) `9:06`
- [Khan: Wilson's Fourteen Points](https://www.khanacademy.org/humanities/ap-us-history/period-7/apush-world-war-i-military-and-diplomacy-lesson/v/woodrow-wilson-s-fourteen-points) `11:04`
- [Heimler: Progressive Era](https://youtu.be/0re-kW3fjdc) `6:19`
- [Heimler: Depression & New Deal](https://youtu.be/dLkYAXOlC0Q) `5:20`
- [Tom Richey: Foreign Policy Between WWI & WWII](https://youtu.be/iB-qkupAFaI) `4:47`
- [Heimler: WWII](https://youtu.be/7NS6dGhP8U4) `3:52`
- [Khan: American Identity](https://www.khanacademy.org/humanities/ap-us-history/period-7/apush-comparison-in-the-period-from-1890-to-1945-lesson/v/shaping-american-national-identity-from-1890-to-1945) `5:40`
- [Tom Richey: Jefferson vs. Roosevelt](https://youtu.be/Lt5Q-bT9K2s) `6:35`
- [Tom Richey: Progressive Amendments](https://youtu.be/aDP3JUC1tdk) `11:02`
- [Tom Richey Prep](https://youtu.be/BAfhZPlgl9Q?t=204) `35:00`
- [AP Review](https://youtu.be/UzZc_Ois6js) `22:02`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-history/period-7/quiz/period-7-quiz-4?modal=1) `5:00`

### Unit 8: The Postwar Period and Cold War (1945-1980)
#### *May 3 | 10%–17%*
The Cold War and the Red Scare  
America as a world power  
The Vietnam War  
The Great Society  
The African American civil rights movement  
Youth culture of the 1960s  
- [Fiveable](https://library.fiveable.me/apush/unit-8) `25:00`
- [Heimler: Cold War](https://youtu.be/WR-7OGfPKwk) `5:54`
- [Crash Course: Cold War in Asia](https://youtu.be/Y2IcmLkuhG0) `13:41`
- [Heimler: The Red Scare](https://youtu.be/ZBYdkjcmeMM) `2:49`
- [Heimler: Great Society](https://youtu.be/UTV9a88IO1k) `2:46`
- [Tom Richey: Nixon's Foreign Policy](https://youtu.be/45IWnKbrdTY) `14:33`
- [Crash Course: Civil Rights](https://youtu.be/S64zRnnn4Po) `11:57`
- [Heimler: Youth Culture](https://youtu.be/-KIdRJwmoWs) `2:51`
- [Crash Course: Ford & Carter](https://youtu.be/pyN5LPHEQ_0) `13:23`
- [Khan: GI Bill](https://www.khanacademy.org/humanities/ap-us-history/period-8/apush-economy-from-1945-to-1960-lesson/a/the-gi-bill) `5:00`
- [Khan: Cuban Missile Crisis](https://www.khanacademy.org/humanities/ap-us-history/period-8/apush-america-as-a-world-power-lesson/v/cuban-missile-crisis) `12:40`
- [Khan: Continuity & Change](https://www.khanacademy.org/humanities/ap-us-history/period-8/continuity-and-change-over-time-in-1945-to-1980/v/continuity-and-change-in-the-postwar-era) `6:51`
- [Tom Richey Prep](https://youtu.be/9KOzA4DoPKs?t=300) `28:35`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-history/period-8/test/period-8-unit-test?modal=1) `10:00`
- [After WWII Notes](https://docs.google.com/document/d/1mTh8A2x7dTA3zhuf6GQbAnLPhm2ydykf-ftWZ1WNKH8/edit?usp=sharing)

### Unit 9: Entering into the 21st Century (1980-Present)
#### *May 4 | 4%–6%*
Reagan and conservatism  
The end of the Cold War  
Shifts in the economy  
Migration and immigration  
Challenges of the 21st century  
- [Fiveable](https://library.fiveable.me/apush/unit-9) `25:00`
- [Crash Course: Reagan](https://youtu.be/2h4DkpFP_aw) `9:33`
- [Heimler: End of Cold War](https://youtu.be/__WPDcQHlpE) `3:42`
- [Heimler: Changing Economy](https://youtu.be/sh8TkdYPadE) `3:47`
- [Khan: Bill Clinton](https://www.khanacademy.org/humanities/ap-us-history/period-9/apush-1990s-america/a/bill-clinton-as-president) `5:00`
- [Crash Course: Terrorism & Bush](https://youtu.be/nlsnnhn3VWE) `10:18`
- [Khan: Great Recession](https://www.khanacademy.org/humanities/ap-us-history/period-9/apush-challenges-of-the-21st-century/a/the-great-recession) `5:00`
- [Heimler: Challenges of 21st Century](https://youtu.be/4ZgXsLT2QZw) `3:56`
- [Khan: Causation](https://www.khanacademy.org/humanities/ap-us-history/period-9/apush-causation-in-the-period-from-1980-to-present-lesson/v/causation-from-1980-2020) `6:28`
- [Tom Richey Prep](https://youtu.be/DCjrU_ojtJw?t=275) `27:30`
- [AP Review](https://youtu.be/UQiyGENeBSE) `25:31`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-history/period-9/test/period-9-unit-test?modal=1) `9:00`

***

## Final Review
#### *May 5*
- [DBQ & LEQ Template](https://www.youtube-nocookie.com/embed/nrk8jkqSdK0?start=1914&end=2653) `12:12`
- [Tom Review](https://www.youtube.com/watch?v=QZNh7FxqR8w)
- [Richey: The 1920s](https://youtu.be/htFYoCicFjg?t=408) `1:24:17`
- [Causes of Great Depression](https://docs.google.com/document/d/1vSe12UcomzPNlt_-WMY3sN5B3P7a9sczGIus0NbnAp0/edit?usp=sharing)
- [Richey: The Great Depression](https://youtu.be/xRcDBoG03tg?t=514) `1:27:47`
- [Richey: The New Deal](https://youtu.be/e7Lt5hYkhGY?t=243) `1:27:50`
- [Declaration of Independence in London](https://youtu.be/mEuE0f8Wr8M) `8:34`
- [Immigration & Migration](https://www.tomrichey.net/uploads/3/2/1/0/32100773/apush_immigration_review_notes.pdf)
- [Marco Study Guide](https://marcolearning.com/wp-content/uploads/2021/04/APUSH-Study-Guide-Pack-2021-v2.pdf)
- [Historical Comparisons](https://docs.google.com/document/d/18jnmCGjMj3sweVmbqGfPR9RsULDojM_le8yLUW87OfU/edit?usp=sharing)
- [Timeline](https://www.sutori.com/en/story/ap-us-history-complete-timeline--dRpue3Fo9HJZ1zG4RWt4rEW7)
- [Presidents & Periods](https://www.trumanlibrary.gov/public/APUSH-Presidents.pdf)
- [Textbook Notes](https://www.apnotes.net/ap-17e.html)
- [2021 FRQs](https://apcentral.collegeboard.org/pdf/ap21-frq-us-history.pdf)
- [Review](https://docs.google.com/document/d/1ou05wQSAVbShcMl7Df8B305f7s5hBrzPfyaGhLAl2VE/edit?usp=sharing)
- [Cram Chart](https://cdn.shopify.com/s/files/1/0508/5878/6976/files/AP_US_History_Cram_Chart_2021.pdf?v=1613614573)
- [Periods & Themes 1](https://cdn.kastatic.org/ka-perseus-images/d42042562db5ba6ca6e2827a71dba4a7b50413a4.png)
- [Periods & Themes 2](https://cdn.kastatic.org/ka-perseus-images/3886ac6bb008e48f5c9a8037be7b98d8071ac766.png)
- [Practice Tests](https://www.crackap.com/ap/us-history/)
- [MCQ Practice](https://youtu.be/KdOtBBKKy4g?t=19)

### AP Test
#### May 6 @ 8am