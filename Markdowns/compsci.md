# AP COMP SCI PANIC TIME

### Big Idea 1: Creative Development
#### *10%-13%*
You’ll learn how important collaboration is in developing programs and how to use an iterative process in your work.  
Collaboration  
Program design and development
- []

### Big Idea 2: Data
#### *17%–22%*
You’ll explore how computers handle data and how data can be used to produce new information and solve problems.  
Data compression  
Extracting information from data  
- []

### Big Idea 3: Algorithms and Programming
#### *30%–35%*  
You’ll learn how to use algorithms and abstractions to create programs that solve problems or to express your own creativity.  
Developing algorithms  
Simulations  
Algorithmic efficiency  
- []

### Big Idea 4: Computer Systems and Networks
#### *11%–15%*
You’ll explore how computer systems and networks work and how using multiple computers to divide tasks can speed up processes.  
The Internet  
Parallel and distributed computing  
- []

### Big Idea 5: Impact of Computing
#### *21%–26%*
You’ll examine the effects computing has had on societies, economies, and cultures and consider the legal and ethical responsibilities of programmers.  
The digital divide  
Computing bias  
Safe computing  
- []

## Final Review
#### *May 2*
- []

### AP Test
#### May 9 @ 12pm