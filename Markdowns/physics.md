# AP Physics

### Unit 1: Kinematics
#### *May 3 | 12%–18%*
Position, velocity, and acceleration  
Representations of motion  
- [Fiveable](https://library.fiveable.me/ap-physics-1/unit-1?q=study-guides)
- [Polychoron: Review](https://youtu.be/9adNqazSrpQ?t=15) `5:10`
- [Khan: Displacement](https://www.khanacademy.org/science/physics/one-dimensional-motion/displacement-velocity-time/v/displacement-from-time-and-velocity-example) `5:00`
- [Khan: Plotting displacement, acceleration, and velocity](https://www.khanacademy.org/science/ap-college-physics-1/xf557a762645cccc5:kinematics-and-introduction-to-dynamics/xf557a762645cccc5:representations-of-motion/v/plotting-projectile-displacement-acceleration-and-velocity) `16:18`
- [Khan: Distance as area](https://www.khanacademy.org/science/ap-college-physics-1/xf557a762645cccc5:kinematics-and-introduction-to-dynamics/xf557a762645cccc5:representations-of-motion/v/why-distance-is-area-under-velocity-time-line) `9:25`
- [Khan: g as gravitational field](https://www.khanacademy.org/science/ap-college-physics-1/xf557a762645cccc5:kinematics-and-introduction-to-dynamics/xf557a762645cccc5:the-gravitational-field/v/viewing-g-as-the-value-of-earth-s-gravitational-field-near-the-surface) `7:31`
- [Khan: Unit Test](https://www.khanacademy.org/science/ap-college-physics-1/xf557a762645cccc5:kinematics-and-introduction-to-dynamics/xf557a762645cccc5:contact-forces/test/xf557a762645cccc5:kinematics-and-introduction-to-dynamics-unit-test) `18:00`

### Unit 2: Dynamics
#### *May 4 | 16%–20%*
Systems
The gravitational field  
Contact forces  
Newton’s First Law  
Newton’s Third Law and free-body diagrams  
Newton’s Second Law  
Applications of Newton’s Second Law  
- [Fiveable](https://library.fiveable.me/ap-physics-1/unit-2?q=study-guides)
- [Khan: Unit Test](https://www.khanacademy.org/science/ap-college-physics-1/xf557a762645cccc5:newtons-laws/xf557a762645cccc5:applications-of-newton-s-second-law/test/xf557a762645cccc5:newtons-laws-unit-test?referrer=upsell) `18:00`

### Unit 3: Circular Motion and Gravitation
#### *May 5 | 6%–8%*
Vector fields  
Fundamental forces  
Gravitational and electric forces  
Gravitational field/acceleration due to gravity on different planets  
Inertial vs. gravitational mass  
Centripetal acceleration vs. centripetal force  
Free-body diagrams for objects in uniform circular motion  
- [Fiveable](https://library.fiveable.me/ap-physics-1/unit-3?q=study-guides)qXNJngvTo) `17:09`
- [Khan: Unit Test](https://www.khanacademy.org/science/ap-college-physics-1/xf557a762645cccc5:circular-motion-and-gravitation/xf557a762645cccc5:applications-of-circular-motion-and-gravitation/test/xf557a762645cccc5:circular-motion-and-gravitation-unit-test?referrer=upsell) `18:00`

### Unit 4: Energy
#### *May 6 | 20%–28%*
Open and closed systems: Energy  
Work and mechanical energy  
Conservation of energy, the work–energy principle, and power  
- [Fiveable](https://library.fiveable.me/ap-physics-1/unit-4?q=study-guides)

### Unit 5: Momentum
#### *May 7 | 12%–18%*
Momentum and impulse  
Representations of changes in momentum  
Open and closed systems: momentum  
Conservation of linear momentum  
- [Fiveable](https://library.fiveable.me/ap-physics-1/unit-5?q=study-guides)
- [Khan: Unit Test](https://www.khanacademy.org/science/ap-college-physics-1/xf557a762645cccc5:energy-and-momentum/xf557a762645cccc5:conservation-of-linear-momentum/test/xf557a762645cccc5:energy-and-momentum-unit-test?referrer=upsell) `18:00`

### Unit 6: Simple Harmonic Motion
#### *May 8 | 4%–6%*
Period of simple harmonic oscillators  
Energy of a simple harmonic oscillator  
- [Fiveable](https://library.fiveable.me/ap-physics-1/unit-6?q=study-guides)

### Unit 7: Torque and Rotational Motion
#### *May 9 | 12%–18%*
Rotational kinematics  
Torque and angular acceleration  
Angular momentum and torque  
Conservation of angular momentum  
- [Fiveable](https://library.fiveable.me/ap-physics-1/unit-7?q=study-guides)
- [Khan: Unit Test](https://www.khanacademy.org/science/ap-college-physics-1/xf557a762645cccc5:simple-harmonic-motion-and-rotational-motion/xf557a762645cccc5:conservation-of-angular-momentum/test/xf557a762645cccc5:simple-harmonic-motion-and-rotational-motion-unit-test?referrer=upsell) `18:00`


***

## Final Review
#### *May 10*
- [Paragraph Length Response](https://library.fiveable.me/ap-physics-1/free-response-questions-frqs/physics-1-paragraph-length-response/study-guide/4udkWTtIs5AgVHj2S2UX) `15:00`
- [Sample MCQ Unpack](https://youtu.be/87hpqlEifwU?t=59) `12:00`
- [Tsao: Overview Video](https://www.youtube.com/watch?v=eCp09HC6rkU) `10:03`
- [Formulas to Memorize](https://www.youtube.com/watch?v=d5qwfMPoN4k) `12:23`
- [Practice Tests](https://www.crackap.com/ap/physics-1/)

### AP Test
#### May 11 @ 12pm