# AP LANG PANIC TIME

### Skill 1: Rhetorical Situation: Reading
#### *11%–14%*
Explain how writers’ choices reflect the components of the rhetorical situation.  

### Skill 2: Rhetorical Situation: Writing
#### *11%–14%*
Make strategic choices in a text to address a rhetorical situation.  

### Skill 3: Claims and Evidence: Reading
#### *13%–16%*
Identify and describe the claims and evidence of an argument.  

### Skill 4: Claims and Evidence: Writing
#### *11%–14%*
Analyze and select evidence to develop and refine a claim.  

### Skill 5: Reasoning and Organization: Reading
#### *13%–16%*
Describe the reasoning, organization, and development of an argument.  

### Skill 6: Reasoning and Organization: Writing
#### *11%–14%*
Use organization and commentary to illuminate the line of reasoning in an argument.  

### Skill 7: Style: Reading
#### *11%–14%*
Explain how writers’ stylistic choices contribute to the purpose of an argument.  

### Skill 8: Style: Writing
#### *11%–14%*
Select words and use elements of composition to advance an argument.  

### Videos
- [AP Rhetorical Analysis Reading](https://youtu.be/_el8xEuPBpU?t=212) `1:05:46`
- [AP Writing MCQs](https://youtu.be/454sX2d2JU0) `53:07`

## Final Review
- [Scoring Guidelines](https://apcentral.collegeboard.org/pdf/ap21-sg-english-language.pdf)
- [2021 FRQs](https://apcentral.collegeboard.org/pdf/ap21-frq-english-language.pdf)
- [2019 FRQs](https://apstudents.collegeboard.org/ap/pdf/ap19-frq-english-language.pdf)
- [Practice Tests](https://www.crackap.com/ap/english-language-and-composition/)


## GENERAL ADVICE:

* Just aim for **thesis** in intro
   * 3 or 4 sentences. (what, how, and why)
* Get as many commentary points as possible:
   * make **3 separate** claims 
   * evidence and commentary to back up each 
   * (a body paragraph will be: claim, evidence, commentary. 5-6 sentences max).
   * Quote words from the passage, NOT entire sentences
* Conclusion is optional, use only for sophistication
   * When running out of time, focus on good body paragraphs

STRUCTURE:  
* **Synthesis**
   * Connect sources together, tie 'em up
   * Be sure to cite! ex. (Source 1) 
* Paragraph 1: Thesis
* 2-4: Claim. Evidence. 2nd Claim. Evidence. Reasoning.
* 5: Conclusion (Limitations/implications)

* **Rhetorical Analysis**
   * What? = Claim
   * How? = Rhe. Choices
   * Why? = Effects/Purpose
* Paragraph 1: Thesis
* 2-4: Claim. Choice (Evidence). Effectiveness. Purpose. Relation.
* 5: Conclusion (Significance/Relevance of choices AND/OR Explain purpose of passage complexities)

* **Argumentative**
* Paragraph 1: Thesis
* 2-4: Claim. Evidence. Reasoning.
* 5: Conclusion (Implications/Limitations of own or similar arguments within broader context)

THESIS:  
**Rhetorical Analysis**  
* In this 19XX article by \[author's name\], the author demonstrates \[claim\]. He does this by \[rhetorical choices\]. By speaking in this way, he \[goal accomplished\].
  
**Argumentative & Synthesis**  
* Try sure to include:
   * good/positives
   * difficulties/challenges
   * compromise/solution
* \[Claim\] not only because of \[reasoning\], but also due to \[more reasoning\]. Although it does have some benefits/cons, such as \[counterclaim\], it still needs a lot of work because \[compromise/solution\].

TIPS:  
**Synthesis**  
1. Strong position.
2.  Explain WHY.
3. Use transitional phrases
   1. Not only, but also; yet; even though; on the other hand
4. Do not end paragraphs with a source

**Rhetorical Analysis**  
1. DEVELOP TONE: Guilt trip; call to action; stress; appeal to religion; current situation; connotation; etc.
2. Make many references to the text
3. Rhetorical situation: What is going on? Who is the speaker? Who is the audience? 
4. Connect back the speakers purpose at the end. WHAT is the point of telling us all of this?
5. Figure out type of speech. Inspirational, eulogy etc

***

### AP Test
#### May 10 @ 8am