# AP Gov

### Unit 1: Foundations of American Democracy
#### *April 24 | 5%–22%*
The ideals of democracy as shown in the Declaration of Independence and the Constitution  
Federalist and Anti-Federalist views on central government and democracy  
Separation of powers and “checks and balances”  
The relationship between the states and the federal government (federalism)  
How federalism has been interpreted differently over time  
- [Heimler Review](https://youtu.be/lxotd_zV1hc?t=50) `25:00`
- [Fiveable](https://library.fiveable.me/ap-gov/unit-1?q=study-guides)
- [Heimler: Types of Democracy](https://youtu.be/oB4P3QjTwpw?t=33) `5:00`
- [Heimler: Interpretations of Federalism](https://youtu.be/QMqLCpxPw34) `6:43`
- [Richey: Anti-Federalists](https://youtu.be/tKdCGXtCctw) `7:27`
- [Khan: Principles of Government](https://www.khanacademy.org/humanities/ap-us-government-and-politics/foundations-of-american-democracy/ideals-of-democracy/a/democratic-ideals-in-the-declaration-of-independence-and-the-constitution)
- [Khan: Commerce Clause](https://www.khanacademy.org/humanities/ap-us-government-and-politics/foundations-of-american-democracy/relationship-between-states-and-the-federal-government/v/categorical-grants-mandates-and-the-commerce-clause) `5:02`
- Required Documents
  - [Declaration of Independence](https://www.archives.gov/founding-docs/declaration-transcript)
    - [Heimler](https://youtu.be/BVctfLkuUxk?t=17) `4:00`
    - [LaManna](https://youtu.be/R428fdzLJrY?t=11) `5:00`
  - [Federalist 10](https://guides.loc.gov/federalist-papers/text-1-10#s-lg-box-wrapper-25493273)
    - [Heimler](https://youtu.be/oUeM0oMomI4?t=17) `4:48`
    - [LaManna](https://youtu.be/xObr8pg8uC0) `9:06`
  - [Brutus No. 1](https://docs-of-freedom.s3.amazonaws.com/uploads/document/attachment/440/Brutus_No_1_Excerpts_Annotated_Proof_3__1_.pdf)
    - [Heimler](https://youtu.be/tm-fNMWuJwo) `5:45`
    - [LaManna](https://youtu.be/Yb4SgAXFUtU) `7:29`
  - [Articles of Confederation](https://www.gilderlehrman.org/sites/default/files/inline-pdfs/T-04759.pdf)
    - [Heimler](https://youtu.be/oct-vb367uM) `4:30`
    - [LaManna](https://youtu.be/xvZqoNXA0Qs) `4:40`
  - [U.S. Constitution](https://constitutioncenter.org/media/files/constitution.pdf)
    - [Heimler](https://youtu.be/1MW1eYICTzU) `7:40`
  - [Federalist 51](https://guides.loc.gov/federalist-papers/text-51-60#s-lg-box-wrapper-25493427)
    - [Heimler](https://youtu.be/f6J-mKOGaVQ) `5:09`
    - [LaManna](https://youtu.be/npcpqqLM814) `5:31`
- SCOTUS Cases
  - [McCulloch v Maryland](https://www.oyez.org/cases/1789-1850/17us316)
    - [Heimler](https://youtu.be/weBfbqH835Y) `3:54`
    - [LaManna](https://youtu.be/Eb0Zrl1bAm0) `4:31`
  - [United States v Lopez](https://www.oyez.org/cases/1994/93-1260)
    - [Heimler](https://youtu.be/WnAgQKqc0F0) `4:11`
    - [LaManna](https://youtu.be/S8Ttom_XN74) `4:57`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-government-and-politics/foundations-of-american-democracy/federalism-in-action/test/foundations-of-american-democracy-unit-test?referrer=upsell) `12:00`

### Unit 2: Interactions Among Branches of Government
#### *April 25 | 25%–36%*
The structures, powers, and functions of each house of Congress  
The roles and powers of the president  
The roles and powers of the Supreme Court and other federal courts  
The roles of the federal bureaucracy (departments, agencies, commissions, and government corporations)  
- [Heimler Review](https://youtu.be/FepoztzFG4E?t=49) `40:00`
- [Fiveable](https://library.fiveable.me/ap-gov/unit-2?q=study-guides)
- [Richey: Senate and House](https://youtu.be/iu8xZiqW3yg) `6:55`
- [Khan: Gerrymandering](https://www.khanacademy.org/humanities/ap-us-government-and-politics/interactions-among-branches-of-government/congressional-behavior/v/gerrymandering) `6:00`
- [Khan: Earmarks, pork barrel, logrolling](https://www.khanacademy.org/humanities/ap-us-government-and-politics/interactions-among-branches-of-government/structures-powers-and-functions-of-congress/v/earmarks-pork-barrel-projects-and-logrolling) `9:34`
- [Heimler: Checks on Presidency](https://youtu.be/sCCQ9MW7Y0o) `3:53`
- [Heimler: Federal Power](https://youtu.be/F54YZYYEtWc) `6:43`
- [Khan: Checks on Judicial](https://www.khanacademy.org/humanities/ap-us-government-and-politics/interactions-among-branches-of-government/checks-on-the-judicial-branch/v/state-checks-on-the-judicial-branch) `6:44`
- [Khan: Iron triangles](https://www.khanacademy.org/humanities/ap-us-government-and-politics/interactions-among-branches-of-government/the-bureaucracy/v/iron-triangles-and-issue-networks) `3:42`
> Article I: Senate and House  
> Article II: President  
> Article III: Judicial Branch  
- Required Documents
  - Federalist 70
    - [Heimler](https://youtu.be/M37VDCB_6WI) `4:12`
    - [LaManna](https://www.youtube.com/watch?v=LJgXaHhGs-E&list=PLHwEFig3yI1YoJzKDWxguVy8IIQWPkk-w&index=7) `7:02`
  - Federalist 78
    - [Heimler](https://youtu.be/2_udhlFNNBk) `4:49`
    - [LaManna](https://www.youtube.com/watch?v=RWEyfFDp6No&list=PLHwEFig3yI1YoJzKDWxguVy8IIQWPkk-w&index=8) `7:01`
- SCOTUS Cases
  - Baker v Carr
    - [Heimler](https://youtu.be/vBc-4aET6f8) `3:20`
    - [LaManna](https://www.youtube.com/watch?v=ay3Gmn1KsBk&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=4) `4:44`
  - Shaw v Reno
    - [Heimler](https://www.youtube.com/watch?v=r2kfKBdG7_g&list=PLEHRHjICEfDV9C4DkO3ma8bjeGBlr8mMz&index=6) `3:57`
    - [LaManna](https://www.youtube.com/watch?v=BXDTQOHAT2E&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=5) `5:51`
  - Marbury v Madison
    - [Heimler](https://youtu.be/GBvUFlqlsyk?t=16) `6:00`
    - [LaManna](https://www.youtube.com/watch?v=Rbh2FCxefPM&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=6) `4:32`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-government-and-politics/interactions-among-branches-of-government/policy-and-the-branches/test/interactions-among-branches-of-government-unit-test?referrer=upsell) `30:00`

### Unit 3: Civil Liberties and Civil Rights
#### *April 27 | 13%-18%*
The intent of the Bill of Rights  
The First Amendment (freedom of speech, freedom of religion, and freedom of the press) and how the Supreme Court has interpreted it  
The Second Amendment (the right to bear arms) and how the Supreme Court has interpreted it  
Supreme Court interpretations of other amendments  
How the due process and equal protection clauses of the Fourteenth Amendment have motivated social movements  
- [Fiveable](https://library.fiveable.me/ap-gov/unit-3?q=study-guides)
- [Khan: Individual freedom vs order and safety](https://www.khanacademy.org/humanities/ap-us-government-and-politics/civil-liberties-and-civil-rights/amendments-balancing-individual-freedom-with-public-order-and-safety/a/lesson-summary-balancing-individual-freedom-with-public-order-and-safety)
- [Heimler: Social Movements](https://www.youtube.com/watch?v=Tb6h_1eMQXE&list=PLEHRHjICEfDUuUGjXakQSIC2kMzRQPvOJ&index=19) `7:30`
- [Khan: Majority vs minority](https://www.khanacademy.org/humanities/ap-us-government-and-politics/civil-liberties-and-civil-rights/balancing-minority-majority-rights/a/lesson-summary-balancing-minority-and-majority-rights)
- Required Documents
  - Bill of Rights
    - [Heimler](https://www.youtube.com/watch?v=R2h8CrX84Eo&list=PLEHRHjICEfDUuUGjXakQSIC2kMzRQPvOJ&index=2) `4:30`
  - Letter from a Birmingham Jail
    - [Heimler](https://youtu.be/EpTvl7MvWjM) `5:48`
    - [LaManna](https://www.youtube.com/watch?v=etkY3DRzPCI&list=PLHwEFig3yI1YoJzKDWxguVy8IIQWPkk-w&index=9) `6:59`
- SCOTUS Cases
  - Engel v Vitale
    - [Heimler](https://www.youtube.com/watch?v=2MT-gmX7_k4&list=PLEHRHjICEfDUuUGjXakQSIC2kMzRQPvOJ&index=4) `3:28`
    - [LaManna](https://www.youtube.com/watch?v=mJif3UoT3Nw&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=7) `2:52`
  - Wisconsin v Yoder
    - [Heimler](https://www.youtube.com/watch?v=SCTxbbGS8AY&list=PLEHRHjICEfDUuUGjXakQSIC2kMzRQPvOJ&index=5) `2:40`
    - [LaManna](https://www.youtube.com/watch?v=O4voJK1jpYw&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=8) `2:55`
  - Tinker v Des Moines
    - [Heimler](https://www.youtube.com/watch?v=YaGFAv03k-0&list=PLEHRHjICEfDUuUGjXakQSIC2kMzRQPvOJ&index=7) `3:26`
    - [LaManna](https://www.youtube.com/watch?v=Tr5yQFO5HyQ&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=9) `5:35`
  - Schenck v U.S.
    - [Heimler](https://www.youtube.com/watch?v=xkGs-Dpaf6w&list=PLEHRHjICEfDUuUGjXakQSIC2kMzRQPvOJ&index=8) `3:31`
    - [LaManna](https://www.youtube.com/watch?v=MK5F8v5YJDM&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=10) `3:56`
  - New York Times v U.S.
    - [Heimler](https://www.youtube.com/watch?v=m_QK4cBsM5g&list=PLEHRHjICEfDUuUGjXakQSIC2kMzRQPvOJ&index=11) `3:12`
    - [LaManna](https://www.youtube.com/watch?v=orjm90A1MtM&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=11) `2:52`
  - McDonald v Chicago
    - [Heimler](https://www.youtube.com/watch?v=nsdhV9fDFhY&list=PLEHRHjICEfDUuUGjXakQSIC2kMzRQPvOJ&index=13) `3:10`
    - [LaManna](https://www.youtube.com/watch?v=DxfcCKC2y-4&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=13) `3:57`
  - Gideon v Wainwright
    - [Heimler](https://www.youtube.com/watch?v=P-Ry5axDF40&list=PLEHRHjICEfDUuUGjXakQSIC2kMzRQPvOJ&index=16) `3:11` 
    - [LaManna](https://www.youtube.com/watch?v=OGIHQGwEkaM&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=12) `3:24`
  - ~~Roe v Wade~~ (not this year)
  - Brown v Board of Education
    - [Heimler](https://www.youtube.com/watch?v=neGpIQQAMKI&list=PLEHRHjICEfDUuUGjXakQSIC2kMzRQPvOJ&index=22) `3:25`
    - [LaManna](https://www.youtube.com/watch?v=5LgnTuPH5K0&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=15) `3:16`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-government-and-politics/civil-liberties-and-civil-rights/affirmative-action/test/civil-liberties-and-civil-rights-unit-test?referrer=upsell) `26:00`

### Unit 4: American Political Ideologies and Beliefs
#### *April 28 | 10%–15%*
How cultural and social factors affect citizens' beliefs about government  
How polls are used to gather data about public opinion  
The ideologies of the Democratic and Republican parties  
How political ideologies affect policy on economic and social issues  
- [LaManna: Review](https://youtu.be/9PoFBs4P9FA) `13:55`
- [Fiveable](https://library.fiveable.me/ap-gov/unit-4?q=study-guides)
- [Heimler: American Attitudes](https://youtu.be/90cDHMOFZ_o)
- [Khan: Poltiical socialization](https://www.khanacademy.org/humanities/ap-us-government-and-politics/american-political-ideologies-and-beliefs/political-socialization/v/political-socialization) `3:32`
- [Heimler: Changes in Ideology](https://www.youtube.com/watch?v=uZrdZZfUTJg&list=PLEHRHjICEfDVOKk-KBKtBYnXEQr5AWL2t&index=4) `5:41`
- [Heimler: Measuing Public Opinion](https://www.youtube.com/watch?v=HmW_WN_Bmhs&list=PLEHRHjICEfDVOKk-KBKtBYnXEQr5AWL2t&index=6) `5:40`
- [Khan: Measuring Public Opinion](https://www.khanacademy.org/humanities/ap-us-government-and-politics/american-political-ideologies-and-beliefs/measuring-public-opinion/a/lesson-summary-measuring-public-opinion)
- [Heimler: Evaluating Public Opinion](https://www.youtube.com/watch?v=iVAAgO4YPA0&list=PLEHRHjICEfDVOKk-KBKtBYnXEQr5AWL2t&index=7) `6:02`
- [Heimler: Ideologies of Parties](https://www.youtube.com/watch?v=rf4vhrh8hjU&list=PLEHRHjICEfDVOKk-KBKtBYnXEQr5AWL2t&index=8) `4:26`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-government-and-politics/american-political-ideologies-and-beliefs/ideology-and-social-policy/test/american-political-ideologies-and-beliefs-unit-test?referrer=upsell) `20:00`

### Unit 5: Political Participation
#### *April 29 | 20%–27%*
Laws that protect the right to vote  
Why it’s hard for third parties and independent candidates to succeed  
Interest groups and their influence  
Campaign finance and its role in elections  
The media’s role in elections  
- [Fiveable](https://library.fiveable.me/ap-gov/unit-5?q=study-guides)
- [Heimler: Voting Rights & Behavior](https://youtu.be/uolsFygzbN8) `4:49`
- [Khan: Voter Turnout](https://www.khanacademy.org/humanities/ap-us-government-and-politics/political-participation/voter-turnout/a/lesson-summary-voter-turnout)
- [Heimler: Political Parties Change](https://www.youtube.com/watch?v=ov8SmqLbhAE&list=PLEHRHjICEfDWpUBYzpujtgqPlMYo0-SGw&index=4) `6:03`
- [Heimler: Third Party](https://www.youtube.com/watch?v=aWeAB0l0ElI&list=PLEHRHjICEfDWpUBYzpujtgqPlMYo0-SGw&index=5) `4:28`
- [Heimler: Policy Outcomes](https://www.youtube.com/watch?v=aOIo6Pb04OU&list=PLEHRHjICEfDWpUBYzpujtgqPlMYo0-SGw&index=8) `3:54`
- [Khan: Incumbency Advantage](https://www.khanacademy.org/humanities/ap-us-government-and-politics/political-participation/electing-a-president/v/incumbency-advantage-phenomenon) `4:19`
- [Khan: Primaries](https://www.khanacademy.org/humanities/ap-us-government-and-politics/political-participation/electing-a-president/v/open-primaries-closed-primaries-and-blanket-primaries) `9:00`
- [Khan: Congressional elections](https://www.khanacademy.org/humanities/ap-us-government-and-politics/political-participation/congressional-elections/a/lesson-summary-congressional-elections)
- [Heimler: Modern Campaigns](https://www.youtube.com/watch?v=qlfmZBuaa9c&list=PLEHRHjICEfDWpUBYzpujtgqPlMYo0-SGw&index=11) `4:30`
- [Heimler: Campaign Finance](https://www.youtube.com/watch?v=hT6EBFIe2Cc&list=PLEHRHjICEfDWpUBYzpujtgqPlMYo0-SGw&index=12) `6:49`
- [Khan: Changing Media](https://www.khanacademy.org/humanities/ap-us-government-and-politics/political-participation/changing-media/a/lesson-summary-changing-media)
- SCOTUS Cases
  - Citizens United v FEC
    - [Heimler](https://youtu.be/PJNnskQ1Oho) `3:10`
    - [LaManna](https://www.youtube.com/watch?v=G5s2qn24Gzg&list=PLHwEFig3yI1bU_EbxrIgfaz-n0noBZR5w&index=15) `7:25`
- [Khan: Unit Test](https://www.khanacademy.org/humanities/ap-us-government-and-politics/political-participation/changing-media/test/political-participation-unit-test?referrer=upsell) `26:00`

***

## Final Review
#### *April 30*
- [Fiveable: Required Supreme Court Cases](https://library.fiveable.me/ap-gov/unit-3/required-scotus-cases/study-guide/rWq8ijXsVxicWG4S0iHQ)
- [Marco: SCOTUS Cases](https://marcolearning.com/wp-content/uploads/2021/04/AP-GOV-STUDY-GUIDE-PACK-SCOTUS-2021-v2.pdf)
- [Marco: Foundational Docs](https://marcolearning.com/wp-content/uploads/2020/09/US-GOV-FOUNDATIONAL-DOCS-STUDY-GUIDE-PACK-2021.pdf)
- [Heimler: Argumentative Essay](https://youtu.be/rN4CJBxAWiU?t=24) `9:00`
- [Heimler: Concept Application Question](https://youtu.be/Ulz-iqQIg8U?t=24) `5:00`
- [LaManna: Constitutional Principles](https://www.youtube.com/watch?v=UutkvVVVEIo&list=PLHwEFig3yI1aB4ufU1YGKR8-2EPHCeeNJ) `14:33`
- [Federalism Timeline](https://apgovtimelineproject.netlify.app/)
- [Required Documents Summaries](https://docs.google.com/document/d/1NDYz4j8H2teMQvZd7y5sDN3jtyvN5ypT0r_sBcOVkRY/edit)
- [2022 FRQs 1](https://apcentral.collegeboard.org/media/pdf/ap22-frq-us-gov-pol-set-1.pdf)
  - [Answers](https://apcentral.collegeboard.org/media/pdf/ap22-sg-us-go-po-set-1.pdf)
- [2022 FRQs 2](https://apcentral.collegeboard.org/media/pdf/ap22-frq-us-gov-pol-set-2.pdf)
  - [Answers](https://apcentral.collegeboard.org/media/pdf/ap22-sg-us-go-po-set-2.pdf)
- [Review](https://docs.google.com/document/d/1JhR1d4RowrIV2POOAnYFDafWmuvCrAkWnFav23Otn8A/edit)
- [Cram Chart](https://cdn.shopify.com/s/files/1/0508/5878/6976/files/AP_US_Gov_Politics_Cram_Chart_2021.pdf?v=1614033729)
- [Practice Tests](https://www.crackap.com/ap/us-government-and-politics/)
- [MCQ Practice](https://youtu.be/mwIy8NpBlZc?t=21) `7:00`

### AP Test
#### May 1 @ 8am